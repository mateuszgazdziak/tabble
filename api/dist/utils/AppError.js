"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AppError = /** @class */ (function (_super) {
    __extends(AppError, _super);
    function AppError(status, message) {
        var _this = _super.call(this, message) || this;
        _this.status = status;
        _this.message = message;
        Error.captureStackTrace(_this, _this.constructor);
        return _this;
    }
    AppError.MISSING_REQUIRED_PARAMTER = "Server.500MissingRequiredParamter";
    AppError.INTERNAL_SERVER_ERROR = "Server.500InternalServerError";
    AppError.PAGE_NOT_FOUND = "Server.404PageNotFound";
    AppError.USER_NOT_FOUND = "Server.403UserNotFound";
    AppError.IDEA_NOT_FOUND = "Server.403UserNotFound";
    AppError.FORBIDDEN = "Server.403Forbidden";
    AppError.MISSING_CREDENTIALS = "Server.401MissingCredentials";
    AppError.BAD_REQUEST = "Server.400BadRequest";
    return AppError;
}(Error));
exports.default = AppError;
//# sourceMappingURL=AppError.js.map