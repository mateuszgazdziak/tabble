"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var express_validator_1 = __importDefault(require("express-validator"));
var dotenv_1 = __importDefault(require("dotenv"));
var LoggerStream_1 = __importDefault(require("./app/utils/LoggerStream"));
var utils_1 = require("./app/utils");
var middleware_1 = __importStar(require("./app/middleware"));
var routes_1 = __importStar(require("./app/components/routes"));
var databaseConnection_1 = __importDefault(require("./app/utils/database/databaseConnection"));
process.on("uncaughtException", function (exception) {
    console.log(exception);
    process.exit(1);
});
process.on("unhandledRejection", function (exception) {
    console.log(exception);
    process.exit(1);
});
databaseConnection_1.default
    .then(function () {
    var app = express_1.default();
    dotenv_1.default.config();
    app.disable("x-powered-by");
    app.use(function (req, res, next) {
        res.header("X-XSS-Protection", "1; mode=block");
        res.header("X-Frame-Options", "deny");
        res.header("X-Content-Type-Options", "nosniff");
        next();
    });
    app.use(express_validator_1.default());
    app.use(morgan_1.default("combined", { stream: new LoggerStream_1.default() }));
    middleware_1.applyMiddleware(middleware_1.default, app);
    routes_1.applyRoutes(routes_1.default, app);
    app.use(function (req, res, next) {
        res.status(404).send("Not found");
    });
    var PORT = parseInt(process.env.PORT || "", 10) || 3001;
    app.listen(PORT, function () {
        console.log("Listening at http://localhost:" + PORT);
    });
})
    .catch(function (error) { return utils_1.logger.error(error); });
//# sourceMappingURL=server.js.map