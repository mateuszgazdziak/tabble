"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var utils_1 = require("./../../utils");
var tableEntity_1 = __importDefault(require("./tableEntity"));
var tableRepository;
var initialize = function () {
    var connection = typeorm_1.getConnection();
    tableRepository = connection.getRepository(tableEntity_1.default);
};
exports.getAllTables = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var tables, error_1, errorCode, errorMessage;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                if (typeof tableRepository === "undefined") {
                    initialize();
                }
                return [4 /*yield*/, tableRepository.find()];
            case 1:
                tables = _a.sent();
                res.send({ tables: tables });
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                errorCode = error_1.status || 403;
                errorMessage = error_1.message || "Forbidden";
                res.status(errorCode).json({ message: errorMessage, errors: error_1 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.getTable = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var errors, table, error_2, errorCode, errorMessage;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                req.checkQuery("id", "table id is required").notEmpty().isNumeric;
                errors = req.validationErrors();
                if (errors) {
                    utils_1.logger.error(errors);
                    throw new utils_1.AppError(400, utils_1.AppError.BAD_REQUEST);
                }
                if (typeof tableRepository === "undefined") {
                    initialize();
                }
                return [4 /*yield*/, tableRepository.findOne({
                        where: { id: req.query.id }
                    })];
            case 1:
                table = _a.sent();
                res.status(200).json(table);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                errorCode = error_2.status || 403;
                errorMessage = error_2.message || "Forbidden";
                res.status(errorCode).json({ message: errorMessage, errors: error_2 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.addNewTable = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var errors, table, id, error_3, errorCode, errorMessage;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                req
                    .checkBody("name", "Wrong name")
                    .notEmpty()
                    .isLength({ min: 3, max: 512 });
                errors = req.validationErrors();
                if (errors) {
                    utils_1.logger.error(errors);
                    throw new utils_1.AppError(400, utils_1.AppError.BAD_REQUEST);
                }
                table = new tableEntity_1.default();
                table.name = req.body.name;
                table.shape = {
                    header: ["column1"],
                    tableCells: [["row1"]]
                };
                if (typeof tableRepository === "undefined") {
                    initialize();
                }
                return [4 /*yield*/, tableRepository.save(table)];
            case 1:
                id = (_a.sent()).id;
                res.status(200).json({ message: "New table added", id: id });
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                errorCode = error_3.status || 403;
                errorMessage = error_3.message || "Forbidden";
                res.status(errorCode).json({ message: errorMessage, errors: error_3 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.editTable = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var errors, table, error_4, errorCode, errorMessage;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                req.checkBody("id", "Wrong id").notEmpty().isNumeric;
                errors = req.validationErrors();
                if (errors) {
                    utils_1.logger.error(errors);
                    throw new utils_1.AppError(400, utils_1.AppError.BAD_REQUEST);
                }
                if (typeof tableRepository === "undefined") {
                    initialize();
                }
                return [4 /*yield*/, tableRepository.findOne({
                        where: { id: req.body.id }
                    })];
            case 1:
                table = _a.sent();
                if (!table) return [3 /*break*/, 3];
                table.shape.header = req.body.table.header;
                table.shape.tableCells = req.body.table.tableCells;
                return [4 /*yield*/, tableRepository.save(table)];
            case 2:
                _a.sent();
                res.status(200).json({ message: "Table edited" });
                return [3 /*break*/, 4];
            case 3: throw new utils_1.AppError(400, utils_1.AppError.BAD_REQUEST);
            case 4: return [3 /*break*/, 6];
            case 5:
                error_4 = _a.sent();
                errorCode = error_4.status || 403;
                errorMessage = error_4.message || "Forbidden";
                res.status(errorCode).json({ message: errorMessage, errors: error_4 });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.getCsvData = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var errors, table, tableShape, csv, error_5, errorCode, errorMessage;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                req.checkQuery("id", "table id is required").notEmpty().isNumeric;
                errors = req.validationErrors();
                if (errors) {
                    utils_1.logger.error(errors);
                    throw new utils_1.AppError(400, utils_1.AppError.BAD_REQUEST);
                }
                if (typeof tableRepository === "undefined") {
                    initialize();
                }
                return [4 /*yield*/, tableRepository.findOne({
                        where: { id: req.query.id }
                    })];
            case 1:
                table = _a.sent();
                if (table) {
                    tableShape = table.shape;
                    csv = utils_1.convertDataTableToCsv(tableShape);
                    res.setHeader("Content-disposition", "attachment; filename=data.csv");
                    res.set("Content-Type", "text/csv");
                    res.status(200).send(csv);
                }
                else {
                    throw new utils_1.AppError(400, utils_1.AppError.BAD_REQUEST);
                }
                return [3 /*break*/, 3];
            case 2:
                error_5 = _a.sent();
                errorCode = error_5.status || 403;
                errorMessage = error_5.message || "Forbidden";
                res.status(errorCode).json({ message: errorMessage, errors: error_5 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=tableController.js.map