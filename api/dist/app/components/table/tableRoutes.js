"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tableController_1 = require("./tableController");
exports.default = [
    {
        path: "/api/v0/tables",
        method: "get",
        handler: [tableController_1.getAllTables]
    },
    {
        path: "/api/v0/table",
        method: "get",
        handler: [tableController_1.getTable]
    },
    {
        path: "/api/v0/table",
        method: "post",
        handler: [tableController_1.addNewTable]
    },
    {
        path: "/api/v0/table/edit",
        method: "put",
        handler: [tableController_1.editTable]
    },
    {
        path: "/api/v0/table/csv",
        method: "get",
        handler: [tableController_1.getCsvData]
    }
];
//# sourceMappingURL=tableRoutes.js.map