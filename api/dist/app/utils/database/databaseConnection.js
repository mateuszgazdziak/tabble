"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var typeorm_1 = require("typeorm");
var path_1 = require("path");
var parentDir = path_1.join(__dirname, '../../components');
var connectionOpts = {
    type: 'postgres',
    host: process.env.DB_HOST || 'postgres',
    port: Number(process.env.DB_PORT) || 5432,
    username: process.env.DB_USERNAME || 'tabble',
    password: process.env.DB_PASSWORD || 'tabble',
    database: process.env.DB_NAME || 'tabble',
    entities: [parentDir + "/**/*Entity.js"],
    synchronize: true,
};
var databaseConnection = typeorm_1.createConnection(connectionOpts);
exports.default = databaseConnection;
//# sourceMappingURL=databaseConnection.js.map