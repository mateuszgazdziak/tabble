"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Logger_1 = __importDefault(require("./Logger"));
var LoggerStream = /** @class */ (function () {
    function LoggerStream() {
    }
    LoggerStream.prototype.write = function (message) {
        Logger_1.default.info(message.substring(0, message.lastIndexOf("\n")));
    };
    return LoggerStream;
}());
exports.default = LoggerStream;
//# sourceMappingURL=LoggerStream.js.map