"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var json2csv_1 = require("json2csv");
var AppError_1 = __importDefault(require("./AppError"));
exports.AppError = AppError_1.default;
var Logger_1 = __importDefault(require("./Logger"));
exports.logger = Logger_1.default;
var convertDataTableToCsv = function (tableShape) {
    var header = tableShape.header, tableCells = tableShape.tableCells;
    var myData = tableCells.reduce(function (acc, row) {
        var temp = {};
        for (var i = 0; i < header.length; i++) {
            temp[header[i]] = row[i];
        }
        acc.push(temp);
        return acc;
    }, []);
    var options = {
        fields: header
    };
    var json2csvParser = new json2csv_1.Parser(options);
    return json2csvParser.parse(myData);
};
exports.convertDataTableToCsv = convertDataTableToCsv;
//# sourceMappingURL=index.js.map