"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var winston_1 = __importDefault(require("winston"));
var OPTIONS = {
    file: {
        level: "info",
        filename: "./../logs/app.log",
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 5,
        colorize: false
    },
    console: {
        level: "debug",
        handleExceptions: true,
        json: false,
        colorize: true
    }
};
var logger = winston_1.default.createLogger({
    transports: [
        new winston_1.default.transports.File(OPTIONS.file),
        new winston_1.default.transports.Console(OPTIONS.console)
    ],
    exitOnError: false
});
exports.default = logger;
//# sourceMappingURL=Logger.js.map