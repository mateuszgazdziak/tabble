"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("./common");
exports.applyMiddleware = function (middlewareWrappers, router) {
    for (var _i = 0, middlewareWrappers_1 = middlewareWrappers; _i < middlewareWrappers_1.length; _i++) {
        var wrapper = middlewareWrappers_1[_i];
        wrapper(router);
    }
};
exports.default = [common_1.corsMiddleware, common_1.bodyParserMiddleware];
//# sourceMappingURL=index.js.map