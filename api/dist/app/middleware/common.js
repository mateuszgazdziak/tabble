"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cors_1 = __importDefault(require("cors"));
var body_parser_1 = __importDefault(require("body-parser"));
exports.asyncMiddleware = function (fn) { return function (req, res, next) { return Promise.resolve(fn(req, res, next)).catch(next); }; };
exports.corsMiddleware = function (router) {
    router.use(cors_1.default({ credentials: true, origin: true }));
};
exports.bodyParserMiddleware = function (router) {
    router.use(body_parser_1.default.urlencoded({ extended: true }));
    router.use(body_parser_1.default.json());
};
//# sourceMappingURL=common.js.map