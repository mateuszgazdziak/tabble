const request = require("supertest");

describe("GET /api/v0/tables", () => {
  it("should return 200", done => {
    request("127.0.0.1:3001")
      .get("/api/v0/tables")
      .expect(200, done);
  });
});

describe("GET /api/v0/table?id=1", () => {
  it("should return 200", done => {
    request("127.0.0.1:3001")
      .get("/api/v0/table")
      .query({ id: 23 })
      .expect(200, done);
  });
});

describe("GET /api/v0/table", () => {
  it("should return 400 if not id specified", done => {
    request("127.0.0.1:3001")
      .get("/api/v0/table")
      .expect(400, done);
  });
});

describe("GET /api/v0/table/csv", () => {
  it("should return 200", done => {
    request("127.0.0.1:3001")
      .get("/api/v0/table/csv?id=5")
      .expect(200, done);
  });
});

describe("GET /api/v0/table/csv", () => {
  it("should return 400 if not id specified", done => {
    request("127.0.0.1:3001")
      .get("/api/v0/table/csv")
      .expect(400, done);
  });
});

describe("POST /api/v0/table without payload", () => {
  it("should return 400", done => {
    request("127.0.0.1:3001")
      .post("/api/v0/table")
      .expect(400, done);
  });
});


describe("PUT /api/v0/table/edit without payload", () => {
  it("should return 400", done => {
    request("127.0.0.1:3001")
      .put("/api/v0/table/edit")
      .expect(400, done);
  });
});


describe("PUT /api/v0/table/edit without payload", () => {
  it("should return 403", done => {
    request("127.0.0.1:3001")
      .put("/api/v0/table/edit")
      .send({ id: 23})
      .expect(403, done);
  });
});



describe("POST /api/v0/table too short payload", () => {
  it("should return 403", done => {
    request("127.0.0.1:3001")
      .post("/api/v0/table")
      .send({ name: "tt" })
      .expect(400, done);
  });
});

describe("POST /api/v0/table", () => {
  it("should return 200", done => {
    request("127.0.0.1:3001")
      .post("/api/v0/table")
      .send({ name: "test" })
      .expect(200, done);
  });
});
