export default class AppError extends Error {
  status: number;
  message: string;
  constructor(status: number, message: string) {
    super(message);
    this.status = status;
    this.message = message;
    Error.captureStackTrace(this, this.constructor);
  }

  static readonly MISSING_REQUIRED_PARAMTER =
    "Server.500MissingRequiredParamter";
  static readonly INTERNAL_SERVER_ERROR = "Server.500InternalServerError";
  static readonly PAGE_NOT_FOUND = "Server.404PageNotFound";
  static readonly USER_NOT_FOUND = "Server.403UserNotFound";
  static readonly IDEA_NOT_FOUND = "Server.403UserNotFound";
  static readonly FORBIDDEN = "Server.403Forbidden";
  static readonly MISSING_CREDENTIALS = "Server.401MissingCredentials";
  static readonly BAD_REQUEST = "Server.400BadRequest";
}
