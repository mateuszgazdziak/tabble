import 'reflect-metadata'
import { createConnection, Connection, ConnectionOptions } from 'typeorm'
import { join } from 'path'
const parentDir = join(__dirname, '../../components')

const connectionOpts: ConnectionOptions = {
    type: 'postgres',
    host: process.env.DB_HOST || 'postgres',
    port: Number(process.env.DB_PORT) || 5432,
    username: process.env.DB_USERNAME || 'tabble',
    password: process.env.DB_PASSWORD || 'tabble',
    database: process.env.DB_NAME || 'tabble',
    entities: [`${parentDir}/**/*Entity.js`],
    synchronize: true,
}

const databaseConnection: Promise<Connection> = createConnection(connectionOpts)

export default databaseConnection
