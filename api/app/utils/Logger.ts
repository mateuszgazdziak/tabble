import winston from "winston";

const OPTIONS = {
  file: {
    level: "info",
    filename: "./../logs/app.log",
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  console: {
    level: "debug",
    handleExceptions: true,
    json: false,
    colorize: true
  }
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File(OPTIONS.file),
    new winston.transports.Console(OPTIONS.console)
  ],
  exitOnError: false
});

export default logger;
