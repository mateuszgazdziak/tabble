import { Parser, json2csv } from "json2csv";
import AppError from "./AppError";
import logger from "./Logger";

export type TableShape = {
  header: string[];
  tableCells: Array<Array<string>>;
};

const convertDataTableToCsv = (tableShape: TableShape) => {
  const { header, tableCells } = tableShape;

  const myData = tableCells.reduce((acc, row) => {
    const temp: any = {};
    for (let i = 0; i < header.length; i++) {
      temp[header[i]] = row[i];
    }
    acc.push(temp);
    return acc;
  }, []);

  const options: json2csv.Options<string[]> = {
    fields: header
  };
  const json2csvParser = new Parser(options);
  return json2csvParser.parse(myData);
};

export { AppError, logger, convertDataTableToCsv };
