import {
  getAllTables,
  getTable,
  addNewTable,
  editTable,
  getCsvData
} from "./tableController";

export default [
  {
    path: "/api/v0/tables",
    method: "get",
    handler: [getAllTables]
  },
  {
    path: "/api/v0/table",
    method: "get",
    handler: [getTable]
  },
  {
    path: "/api/v0/table",
    method: "post",
    handler: [addNewTable]
  },
  {
    path: "/api/v0/table/edit",
    method: "put",
    handler: [editTable]
  },
  {
    path: "/api/v0/table/csv",
    method: "get",
    handler: [getCsvData]
  }
];
