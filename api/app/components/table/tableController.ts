import { Request, Response } from "express";
import { getConnection, Repository } from "typeorm";
import {
  AppError,
  logger,
  convertDataTableToCsv,
  TableShape
} from "./../../utils";

import Table from "./tableEntity";
let tableRepository: Repository<Table>;

const initialize = () => {
  const connection = getConnection();
  tableRepository = connection.getRepository(Table);
};

export const getAllTables = async (req: Request, res: Response) => {
  try {
    if (typeof tableRepository === "undefined") {
      initialize();
    }
    const tables = await tableRepository.find();
    res.send({ tables });
  } catch (error) {
    const errorCode = error.status || 403;
    const errorMessage = error.message || "Forbidden";
    res.status(errorCode).json({ message: errorMessage, errors: error });
  }
};

export const getTable = async (req: Request, res: Response) => {
  try {
    req.checkQuery("id", "table id is required").notEmpty().isNumeric;

    let errors = req.validationErrors();
    if (errors) {
      logger.error(errors);
      throw new AppError(400, AppError.BAD_REQUEST);
    }
    
    if (typeof tableRepository === "undefined") {
      initialize();
    }


    const table = await tableRepository.findOne({
      where: { id: req.query.id }
    });

    res.status(200).json(table);
  } catch (error) {
    const errorCode = error.status || 403;
    const errorMessage = error.message || "Forbidden";
    res.status(errorCode).json({ message: errorMessage, errors: error });
  }
};

export const addNewTable = async (req: Request, res: Response) => {
  try {
    req
      .checkBody("name", "Wrong name")
      .notEmpty()
      .isLength({ min: 3, max: 512 });

    let errors = req.validationErrors();
    if (errors) {
      logger.error(errors);
      throw new AppError(400, AppError.BAD_REQUEST);
    }

    const table = new Table();
    table.name = req.body.name;
    table.shape = {
      header: ["column1"],
      tableCells: [["row1"]]
    };

    if (typeof tableRepository === "undefined") {
      initialize();
    }

    const { id } = await tableRepository.save(table);
    res.status(200).json({ message: `New table added`, id });
  } catch (error) {
    const errorCode = error.status || 403;
    const errorMessage = error.message || "Forbidden";
    res.status(errorCode).json({ message: errorMessage, errors: error });
  }
};

export const editTable = async (req: Request, res: Response) => {
  try {
    req.checkBody("id", "Wrong id").notEmpty().isNumeric;

    let errors = req.validationErrors();
    if (errors) {
      logger.error(errors);
      throw new AppError(400, AppError.BAD_REQUEST);
    }

    if (typeof tableRepository === "undefined") {
      initialize();
    }

    const table: Table | undefined = await tableRepository.findOne({
      where: { id: req.body.id }
    });

    if (table) {
      table.shape.header = req.body.table.header;
      table.shape.tableCells = req.body.table.tableCells;


      await tableRepository.save(table);
      res.status(200).json({ message: `Table edited` });
    } else {
      throw new AppError(400, AppError.BAD_REQUEST);
    }
  } catch (error) {
    const errorCode = error.status || 403;
    const errorMessage = error.message || "Forbidden";
    res.status(errorCode).json({ message: errorMessage, errors: error });
  }
};

export const getCsvData = async (req: Request, res: Response) => {
  try {
    req.checkQuery("id", "table id is required").notEmpty().isNumeric;

    let errors = req.validationErrors();
    if (errors) {
      logger.error(errors);
      throw new AppError(400, AppError.BAD_REQUEST);
    }

    if (typeof tableRepository === "undefined") {
      initialize();
    }

    const table: Table | undefined = await tableRepository.findOne({
      where: { id: req.query.id }
    });

    if (table) {
      const tableShape: TableShape = table.shape;

      const csv = convertDataTableToCsv(tableShape);

      res.setHeader("Content-disposition", "attachment; filename=data.csv");
      res.set("Content-Type", "text/csv");
      res.status(200).send(csv);
    } else {
      throw new AppError(400, AppError.BAD_REQUEST);
    }
  } catch (error) {
    const errorCode = error.status || 403;
    const errorMessage = error.message || "Forbidden";
    res.status(errorCode).json({ message: errorMessage, errors: error });
  }
};
