import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
} from "typeorm";
import {TableShape} from "./../../utils"
import { Length } from "class-validator";

@Entity()
export default class Table {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column("text")
  @Length(1, 512)
  name!: string;

  @Column({ type: "json" })
  shape!: TableShape;
}
