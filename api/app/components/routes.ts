import { Router, Request, Response, NextFunction } from "express";
import tableRoutes from "./table/tableRoutes";

type RouteHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => Promise<void> | void;

type Route = {
  path: string;
  method: string;
  handler: RouteHandler | RouteHandler[];
};

export default [...tableRoutes];

export const applyRoutes = (routes: Route[], router: Router) => {
  for (const route of routes) {
    const { method, path, handler } = route;
    (router as any)[method](path, handler);
  }
};
