import React from "react";
import TableList from "./components/TableList/TableList";
import TableView from "./components/TableView/TableView";
import { NewTable } from "./components/AddTable/AddTableView";
import NavBar from "./components/NavBar/NavBar";
import "./App.css";
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect
} from "react-router-dom";

const App: React.FC = () => {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route exact path="/" component={TableList} />
        <Route exact path="/table/:id" component={TableView} />
        <Route exact path="/new" component={NewTable} />
      </Switch>
    </Router>
  );
};

export default App;
