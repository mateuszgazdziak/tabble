import React from "react";
import ReactDOM from "react-dom";
import TableView from "./../TableView/TableView";
import { BrowserRouter } from "react-router-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BrowserRouter>
      <TableView id={3} name="ddd"/>
    </BrowserRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
