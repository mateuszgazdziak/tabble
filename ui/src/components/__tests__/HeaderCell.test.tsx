import React from "react";
import ReactDOM from "react-dom";
import { HeaderCell } from "./../Table/HeaderCell/HeaderCell";
import { BrowserRouter } from "react-router-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BrowserRouter>
      <HeaderCell
        key={3}
        value={"test"}
        columnIndex={1}
        editMode={false}
        handleChangeValue={() => true}
        handleAddColumn={() => true}
        handleDeleteColumn={() => true}
      />
    </BrowserRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
