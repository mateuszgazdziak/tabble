import React from "react";
import ReactDOM from "react-dom";
import Table from "./../Table/Table";
import { BrowserRouter } from "react-router-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BrowserRouter>
      <Table />
    </BrowserRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
