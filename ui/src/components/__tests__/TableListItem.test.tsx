import React from "react";
import ReactDOM from "react-dom";
import TableListItem from "./../TableListItem/TableListItem";
import { BrowserRouter } from "react-router-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BrowserRouter>
      <TableListItem id={3} value="test" />
    </BrowserRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
