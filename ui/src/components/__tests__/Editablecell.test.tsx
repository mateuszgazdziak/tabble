import React from "react";
import ReactDOM from "react-dom";
import { EditableCell } from "./../Table/EditableCell/EditableCell";
import { BrowserRouter } from "react-router-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BrowserRouter>
      <EditableCell
        key={"3"}
        rowIndex={0}
        columnIndex={1}
        columnLabel={"test"}
        value="test"
        handleChangeValue={() => true}
      />
    </BrowserRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
