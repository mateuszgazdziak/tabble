import React, { useState, useEffect } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { tabbleApi } from "./../../utils/api/tabbleaApi";
import TableListItem from "./../TableListItem/TableListItem";
import "./TableList.css";

interface Props extends RouteComponentProps {
  id: number;
  name: string;
}

type TableItem = {
  id: number;
  name: string;
};

const TableList: React.FC<Props> = props => {
  const { history } = props;
  const [tables, setTables] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const result = await tabbleApi.getIAllTables();

        setTables(result.tables);
      } catch (error) {
        setIsError(true);
      }

      setIsLoading(false);
    };

    fetchData();
  }, []);

  const handleRedirectToAddTableView = () => history.push("/new");

  const renderListOfTables = () =>
    tables.map((table: TableItem) => {
      return <TableListItem key={table.id} id={table.id} value={table.name} />;
    });

  const renderContent = () => {
    if (isError) {
      <div>Something went wrong ...</div>;
    } else if (isLoading) {
      return <h2>Loading data...</h2>;
    } else if (!tables.length) {
      return <h2>Click Add Table to add your first table</h2>;
    } else {
      return <ul>{renderListOfTables()}</ul>;
    }
  };
  return (
    <div className="headerWrapper">
      <h2>List of tables</h2>
      {renderContent()}
      <button onClick={handleRedirectToAddTableView} className="newItem">
        Add Table
      </button>
    </div>
  );
};

export default withRouter(TableList);
