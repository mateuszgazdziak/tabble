import React, { useState } from "react";
import { tabbleApi } from "../../utils/api/tabbleaApi";
import { withRouter, RouteComponentProps } from "react-router-dom";
import "./AddTableView.css";

export const NewTable: React.FC<RouteComponentProps> = props => {
  const [tableName, setTableName] = useState("");

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTableName(e.target.value);
  };

  const handleAddNewTable = async () => {
    const result = await tabbleApi.add(tableName);
    props.history.push(`/table/${result.id}`);
  };

  const isDisabled = () => tableName.length < 3;

  const className = isDisabled() ? "disabled" : "addItem";

  return (
    <div className="content">
      <span>
        <input
          type="text"
          placeholder="Type name"
          value={tableName}
          onChange={onChange}
        />
        <p>At least 3 chars.</p>
      </span>
      <button
        className={className}
        disabled={isDisabled()}
        onClick={handleAddNewTable}
      >
        Add New
      </button>
    </div>
  );
};

export default withRouter(NewTable);
