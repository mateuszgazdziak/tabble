import React from "react";

type Props = {
  fill?: string;
  width?: string;
  height?: string;
  className?: string;
  viewBox?: string;
};

const defaultProps: Props = {
  width: "24",
  height: "24",
  viewBox: "0 0 24 24",
  fill: "none"
};

const SVG: React.FC<Props> = props => {
  const { width, height, viewBox, fill } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewBox}
      fill={fill}
      stroke="#e7e8e5"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
      <line x1="12" y1="8" x2="12" y2="16" />
      <line x1="8" y1="12" x2="16" y2="12" />
    </svg>
  );
};

SVG.defaultProps = defaultProps;

export default SVG;
