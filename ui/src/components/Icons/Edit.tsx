import React from "react";

type Props = {
  fill?: string;
  width?: string;
  height?: string;
  className?: string;
  viewBox?: string;
};

const defaultProps: Props = {
  width: "18",
  height: "18",
  viewBox: "0 0 24 24",
  fill: "none"
};

const SVG: React.FC<Props> = props => {
  const { width, height, viewBox, fill } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewBox}
      fill={fill}
      stroke="#e7e8e5"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z" />
    </svg>
  );
};

SVG.defaultProps = defaultProps;

export default SVG;
