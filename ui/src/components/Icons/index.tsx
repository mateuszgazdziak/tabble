import React from "react";

import Add from "./Add";
import Edit from "./Edit";
import Export from "./Export";
import Delete from "./Delete";

type Props = {
  name: string;
  fill?: string;
  width?: string;
  height?: string;
  className?: string;
  viewBox?: string;
};

const Icon: React.FC<Props> = props => {
  switch (props.name) {
    case "add":
      return <Add {...props} />;
    case "edit":
      return <Edit {...props} />;
    case "delete":
      return <Delete {...props} />;
    case "export":
      return <Export {...props} />;
    default:
      return <Add {...props} />;
  }
};

export default Icon;
