import React from "react";
import Icon from "./../../Icons";
import "./HeaderCell.css";

type Props = {
  key: number;
  columnIndex: number;
  value: string;
  editMode: boolean;
  handleChangeValue: (columnIndex: number, newValue: string) => void;
  handleAddColumn: (columnIndex: number) => void;
  handleDeleteColumn: (columnIndex: number) => void;
};

export const HeaderCell: React.FC<Props> = props => {
  const {
    value,
    columnIndex,
    editMode,
    handleChangeValue,
    handleAddColumn,
    handleDeleteColumn
  } = props;

  const addColumn = () => {
    handleAddColumn(columnIndex);
  };

  const deelteColumn = () => {
    handleDeleteColumn(columnIndex);
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    handleChangeValue(columnIndex, e.target.value);
  };

  return !editMode ? (
    <th>
      <input type="text" value={value} onChange={onChange} />
    </th>
  ) : (
    <th>
      <span className="editableHeader">
        <input
          placeholder={"add column name"}
          type="text"
          value={value}
          onChange={onChange}
        />
        <button className="actionButton" onClick={addColumn}>
          {<Icon name="add" />}
        </button>
        <button className="actionButton" onClick={deelteColumn}>
          {<Icon name="delete" />}
        </button>
      </span>
    </th>
  );
};
