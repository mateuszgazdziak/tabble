import React from "react";
import Icon from "../../Icons";
import "./ActionButtonsCell.css";

type Props = {
  rowIndex: number;
  handleAddRow: (rowIndex: number) => void;
  handleDeleteRow: (rowIndex: number) => void;
};

export const ActionButtonsCell: React.FC<Props> = props => {
  const { rowIndex, handleAddRow, handleDeleteRow } = props;

  const addRow = () => {
    handleAddRow(rowIndex);
  };
  const deleteRow = () => {
    handleDeleteRow(rowIndex);
  };

  return (
    <td className="actionButtonsCell">
      <span>
        <button className="actionButton" onClick={addRow}>
          {<Icon fill="#a5e829" name="add" />}
        </button>
        <button className="actionButton" onClick={deleteRow}>
          {<Icon fill="#f44280" name="delete" />}
        </button>
      </span>
    </td>
  );
};
