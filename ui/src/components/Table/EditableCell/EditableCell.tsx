import React from "react";

type Props = {
  key: string;
  rowIndex: number;
  columnIndex: number;
  columnLabel: string;
  value: string;
  handleChangeValue: (row: number, column: number, value: string) => void;
};

export const EditableCell: React.FC<Props> = props => {
  const {
    rowIndex,
    columnIndex,
    value,
    columnLabel,
    handleChangeValue
  } = props;

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    handleChangeValue(rowIndex, columnIndex, e.target.value);
  };

  return (
    <td data-label={columnLabel}>
      <input
        type="text"
        key={`${rowIndex}_${columnIndex}`}
        value={value}
        onChange={onChange}
        placeholder="type something :)"
      />
    </td>
  );
};
