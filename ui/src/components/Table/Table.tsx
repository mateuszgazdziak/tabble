import React, { useState, useReducer, useEffect } from "react";
import { ActionButtonsCell } from "./ActionButtonsCell/ActionButtonsCell";
import { EditableCell } from "./EditableCell/EditableCell";
import { HeaderCell } from "./HeaderCell/HeaderCell";
import { tabbleApi } from "./../../utils/api/tabbleaApi";
import Icon from "./../Icons";
import "./Table.css";
import { withRouter, RouteComponentProps } from "react-router-dom";

export type TableShape = {
  header: string[];
  tableCells: Array<Array<string>>;
};

type QueryParams = {
  id: string;
};

const Table: React.FC<RouteComponentProps<QueryParams>> = props => {
  const [isInEditMode, setIsEditMode] = useState<boolean>(false);
  const [header, setHeader] = useState<string[]>([]);
  const [tableCells, setTableCells] = useState<Array<Array<string>>>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [showCannotDelete, setShowCannotDelete] = useState(false);

  const id = props.match.params.id;

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setIsError(false);

      try {
        const result = await tabbleApi.getTable(id);

        const tableShape: TableShape = result.shape;

        setHeader(tableShape.header);
        setTableCells(tableShape.tableCells.slice());
      } catch (error) {
        setIsError(true);
      }

      setIsLoading(false);
    };

    fetchData();
  }, []);

  const getEmptyRow = (size: number) => Array<string>(size);

  const toggleEditMode = () => setIsEditMode(!isInEditMode);

  const handleExportToCsv = async () => {
    const result = await tabbleApi.exportCsv(id);
    const url = window.URL.createObjectURL(new Blob([result]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", "data.csv");
    document.body.appendChild(link);
    link.click();
  };

  const handleExit = () => {
    setIsEditMode(false);
    return props.history.push(`/table/${id}`);
  };

  const saveTableUpates = async () => {
    try {
      await tabbleApi.edit(+id, { header, tableCells });
    } catch (error) {
      setIsError(true);
    }
  };

  const updateHeader = async (column: number, newValue: string) => {
    header[column] = newValue;
    setHeader(header.slice());

    saveTableUpates();
  };

  const updateCell = async (row: number, column: number, newValue: string) => {
    tableCells[row][column] = newValue;
    setTableCells(tableCells.slice());

    saveTableUpates();
  };

  const handleAddColumn = async (index: number) => {
    tableCells.forEach(row => row.splice(index, 0, ""));
    header.splice(index, 0, "");

    setHeader(header.slice());
    setTableCells(tableCells.slice());

    saveTableUpates();
  };

  const handleDeleteColumn = async (index: number) => {
    if (header.length > 1 && tableCells.length > 1) {
      header.splice(index, 1);
      tableCells.forEach(row => row.splice(index, 1));

      setHeader(header.slice());
      setTableCells(tableCells.slice());

      saveTableUpates();
    } else {
      setShowCannotDelete(true);
    }
  };

  const handleAddRow = async (index: number) => {
    tableCells.splice(index, 0, getEmptyRow(header.length));
    setTableCells(tableCells.slice());

    saveTableUpates();
  };
  const handleDeleteRow = async (index: number) => {
    if (header.length > 1 && tableCells.length > 1) {
      tableCells.splice(index, 1);

      setTableCells(tableCells.slice());

      saveTableUpates();
    } else {
      setShowCannotDelete(true)
    }
  };

  const renderHeader = () => {
    const headerCells = Array<JSX.Element>();
    for (let column = 0; column < header.length; column++) {
      headerCells.push(
        <HeaderCell
          key={column}
          columnIndex={column}
          value={header[column]}
          editMode={isInEditMode}
          handleChangeValue={updateHeader}
          handleAddColumn={handleAddColumn}
          handleDeleteColumn={handleDeleteColumn}
        />
      );
    }

    return headerCells;
  };

  const renderRows = () => {
    const rows = Array<JSX.Element>();

    for (let rowIndex = 0; rowIndex < tableCells.length; rowIndex++) {
      rows.push(<tr key={rowIndex}>{renderCells(rowIndex)}</tr>);
    }

    return rows;
  };

  const renderCells = (rowIndex: number) => {
    const cells = [];

    for (
      let columnIndex = 0;
      columnIndex < tableCells[0].length;
      columnIndex++
    ) {
      cells.push(
        <EditableCell
          key={`${columnIndex}_${rowIndex}`}
          rowIndex={rowIndex}
          columnIndex={columnIndex}
          columnLabel={header[columnIndex]}
          value={tableCells[rowIndex][columnIndex] || ""}
          handleChangeValue={updateCell}
        />
      );
    }

    if (isInEditMode) {
      cells.push(
        <ActionButtonsCell
          key={rowIndex}
          rowIndex={rowIndex}
          handleAddRow={handleAddRow}
          handleDeleteRow={handleDeleteRow}
        />
      );
    }

    return cells;
  };

  const renderButtons = () => {
    return isInEditMode ? (
      <button
        className="tabbleButton singleButton exitButton"
        onClick={handleExit}
      >
        Exit
      </button>
    ) : (
      <span className="buttonGroup">
        <button className="tabbleButton editButton" onClick={toggleEditMode}>
          <span>
            <Icon name="edit" />
            Edit
          </span>
        </button>
        <button className="tabbleButton" onClick={handleExportToCsv}>
          <span>
            <Icon name="export" />
            CSV
          </span>
        </button>
      </span>
    );
  };

  const renderTable = () => {
    return (
      <div className="tableContainer">
        <table>
          <thead>
            <tr>{renderHeader()}</tr>
          </thead>
          <tbody>{renderRows()}</tbody>
        </table>
        {renderButtons()}
      </div>
    );
  };

  const renderContent = () => {
    if (isError) {
      <div>Something went wrong ...</div>;
    } else if (showCannotDelete) {
      return <h2>Cannot remove last column or row...</h2>;
    } else if (isLoading) {
      return <h2>Loading data...</h2>;
    } else if (!header.length) {
      return <h2>Click Edit Table to add new columns</h2>;
    } else {
      return renderTable();
    }
  };

  return <div>{renderContent()}</div>;
};

export default withRouter(Table);
