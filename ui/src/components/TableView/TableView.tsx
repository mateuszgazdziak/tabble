import React from "react";
import Table from "../Table/Table";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface Props extends RouteComponentProps {
  id: number;
  name: string;
}
const TableView: React.FC<Props> = props => {
  const { id, name } = props;
  return (
    <div className="tableWrapper">
      <Table />
    </div>
  );
};

export default withRouter(TableView);
