import React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import "./TableListItem.css";

interface Props extends RouteComponentProps {
  id: number;
  value: string;
}

const LINK_REL = "nofollow noreferer noopener";

const TableListItem: React.FC<Props> = props => {
  const { id, value, history } = props;

  const handleOnclick = () => {
    history.push(`/table/${id}`);
  };

  return (
    <li>
      <a className="tableListItem" onClick={handleOnclick} rel={LINK_REL}>
        {value}
      </a>
    </li>
  );
};

export default withRouter(TableListItem);
