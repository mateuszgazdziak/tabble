import axios, { AxiosInstance } from "axios";

export const SERVER_ROOT = process.env.REACT_APP_URL || "localhost";
export const API_ROOT = `http://${SERVER_ROOT}:3001/api/v0`;
const TIMEOUT = 20000;
const HEADERS = {
  "Content-Type": "application/json",
  Accept: "application/json"
};

const handleSuccess = (response: any) => response;
const handleError = (error: Error) => Promise.reject(error);

class ApiService {
  client: AxiosInstance;
  constructor() {
    this.client = axios.create({
      baseURL: API_ROOT,
      timeout: TIMEOUT,
      headers: HEADERS
    });

    this.client.interceptors.response.use(handleSuccess, handleError);
  }

  get(path: string) {
    return this.client.get(path).then((response: any) => response.data);
  }
  post(path: string, payload: object) {
    return this.client
      .post(path, payload)
      .then((response: any) => response.data);
  }
  put(path: string, payload: object) {
    return this.client
      .put(path, payload)
      .then((response: any) => response.data);
  }
  patch(path: string, payload: object) {
    return this.client
      .patch(path, payload)
      .then((response: any) => response.data);
  }
  delete(path: string, payload: object) {
    return this.client
      .delete(path, payload)
      .then((response: any) => response.data);
  }
}

const apiService = new ApiService();

export default apiService;
