import apiService, { API_ROOT } from "./apiService";
import { TableShape } from "../../components/Table/Table";

class TabbleApi {
  getIAllTables() {
    return apiService.get(`${API_ROOT}/tables`);
  }

  getTable(id: string) {
    return apiService.get(`${API_ROOT}/table?id=${id}`);
  }

  exportCsv(id: string) {
    return apiService.get(`${API_ROOT}/table/csv?id=${id}`);
  }

  add(name: string) {
    return apiService.post(`${API_ROOT}/table`, { name });
  }

  edit(id: number, table: TableShape) {
    return apiService.put(`${API_ROOT}/table/edit`, {id, table})
  }
}

export const tabbleApi = new TabbleApi();
